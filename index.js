const slugify= require("slugify");
const express = require("express");
const app= express();
const titulo= "Y14037";

app.set("view engine", "ejs");



const productos=[
	{id:1, nombre:"producto 1 ", precio: 25000},
	{id:2, nombre:"producto 2 ", precio: 70000},
	{id:3, nombre:"producto 3 ", precio: 52000},
];

productos.forEach(productos=>{
    productos.slug=slugify(productos.nombre)
});

app.set("views", "views" );
app.use(express.json());
app.use(express.urlencoded({ extended: true }));



//app.get(("/bienvenida"), (req, res)=>{res.send("Hola Mundo")});
app.get(("/bienvenida"), (req, res)=>{res.render("bienvenida", {titulo:titulo, productos:productos})});

app.get("/prueba-test", (req, res)=>{res.json(productos)});



app.get("/ver_producto/:slug", (req, res)=> {
    const slug= req.params.slug;
    const productoEncontrado=productos.find(producto=>producto.slug===slug)
    res.render("ver_producto", {titulo:titulo, producto: productoEncontrado});
})

app.put("/ver_producto/:slug", (req, res) => {
    const slug = req.params.slug;
    const productoEncontrado = productos.find(producto => producto.slug === slug);
  
    if (productoEncontrado) {
        productoEncontrado.nombre = req.body.nombre;
        productoEncontrado.precio = parseFloat(req.body.precio);
  
        productos.forEach(producto => {
          producto.slug = slugify(producto.nombre);
        });

        res.json({ success: true, slug: productoEncontrado.slug });
    } else {
        res.status(404).send("Producto no encontrado");
    }
});

app.delete("/ver_producto/:slug", (req, res) => {
    const slug= req.params.slug;
    const elimProducto = productos.findIndex(producto => producto.slug === slug);

    if (elimProducto !== -1){
        productos.splice(elimProducto, 1);

        res.status(200).json({mensaje:"Producto eliminado"});
    }else{
        res.status(404).json({mensaje: "Producto no encontrado"});
    };
  
});


app.get("/crear_producto", (req, res) => {
    res.render("crear_producto", { titulo: titulo });
});

app.post("/crear_producto", (req, res) => {
     
    const nuevoProducto = {
        id: (Math.max(...productos.map((producto)=>producto.id))+1),
       
        nombre: req.body.nombre,
        precio: parseFloat(req.body.precio),
        slug: slugify(req.body.nombre)
    };

    productos.push(nuevoProducto);
    res.redirect("/bienvenida");
});




module.exports= app