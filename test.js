const supertest = require("supertest");
const app = require ("./index");
const request = require ("supertest");

test("La respuesta es un JSON", async() =>{

await supertest (app)
.get("/prueba-test")
.expect(200)
.then((response) => {

            expect (response.header["content-type"]).toEqual(
            "application/json; charset=utf-8"
    );

})

});


test("El array tiene la longitud 3", async() =>{

    await supertest (app)
    .get("/prueba-test")
    .expect(200)
    .then((response) => {
    
                expect (response.header["content-type"]).toEqual(
                "application/json; charset=utf-8"
        );
        expect(response.body).toHaveLength(3);
    })
    
    });
    

    test("Verificar el primer elemento del array", async() =>{

        await supertest (app)
        .get("/prueba-test")
        .expect(200)
        .then((response) => {
        
                    expect (response.header["content-type"]).toEqual(
                    "application/json; charset=utf-8"
            );
            expect(response.body[0]).toHaveProperty("id",1);
        })
        
        });

        test("Verificar el ultimo elemento del array", async() =>{

            await supertest (app)
            .get("/prueba-test")
            .expect(200)
            .then((response) => {
            
                        expect (response.header["content-type"]).toEqual(
                        "application/json; charset=utf-8"
                );
                expect(response.body[2]).toHaveProperty("id",3);
            })
            
            });
